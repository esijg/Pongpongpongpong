extends RigidBody2D

# const SPEEDUP = 4
const MAXSPEED = 300

var screen_size
var random_number

var random_number_dir_x
var random_number_dir_y
var number_y
var random_number_x

func _ready():
	screen_size = get_viewport_rect().size
	set_fixed_process(true)
	randomize()
	
	if(get_name()=="MenuBall"):
		random_number_dir_x = (randi()%2)
		random_number_dir_y = (randi()%2)
		random_number_x = (randi()%51)
		number_y = 50-random_number_x

		if(random_number_dir_x == 0):
			random_number_x = random_number_x*-1
		if(random_number_dir_y == 0):
			number_y = number_y*-1

		set_pos(Vector2(screen_size.x/2, screen_size.y/2))
		print(random_number)
		set_linear_velocity(Vector2(random_number_x,number_y))
		print("X:",random_number_x,"    Y:",number_y)
	
func _fixed_process(delta):
	var ball_pos = get_pos()
	if (ball_pos.x < 0):
		random_number = randi()%11-4
		if(random_number == 0):
			random_number += 1
		set_pos(Vector2(screen_size.x/2, screen_size.y/2))
		set_linear_velocity(Vector2(-50,random_number))
	if (ball_pos.x > screen_size.x):
		random_number = randi()%11-4
		if(random_number == 0):
			random_number += 1
		set_pos(Vector2(screen_size.x/2, screen_size.y/2))
		set_linear_velocity(Vector2(50,random_number))
	if (ball_pos.y < 0):
		random_number = randi()%11-4
		if(random_number == 0):
			random_number += 1
		set_pos(Vector2(screen_size.x/2, screen_size.y/2))
		set_linear_velocity(Vector2(random_number,-50))
	if (ball_pos.y > screen_size.y):
		random_number = randi()%11-4
		if(random_number == 0):
			random_number += 1
		set_pos(Vector2(screen_size.x/2, screen_size.y/2))
		set_linear_velocity(Vector2(random_number,50))
	