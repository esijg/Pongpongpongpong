
extends Node2D

var deathcount = 0

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	hide()
	pass

func death(MostRecentDeadPlayer):
	deathcount += 1
	
	if (deathcount == 3):
		if(has_node("../player1") == true && MostRecentDeadPlayer != "player1"):
			get_node("AnimatedSprite").set_frame(0)

		if(has_node("../player2") == true && MostRecentDeadPlayer != "player2"):
			get_node("AnimatedSprite").set_frame(1)

		if(has_node("../player3") == true && MostRecentDeadPlayer != "player3"):
			get_node("AnimatedSprite").set_frame(2)

		if(has_node("../player4") == true && MostRecentDeadPlayer != "player4"):
			get_node("AnimatedSprite").set_frame(3)

		show()