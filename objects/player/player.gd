
extends KinematicBody2D

# member variables here, example:
# var a=2
# var b="textvar"
const PAD_SPEED = 150
const ROT_SPEED = 10
var screen_size
var pad_size

func _ready():
	set_fixed_process(true)
	pad_size = get_node("Sprite").get_texture().get_size()
	screen_size = get_viewport_rect().size

func _fixed_process(delta):
	# move player1 pad
	var player_pos = get_pos()
	var player_rot = get_rot()

	if (get_name() == "player1" or get_name() == "player3"):
		if (player_pos.y > 0+pad_size.y/2+18 and Input.is_action_pressed(get_name()+"_move_minus")):
		    player_pos.y += -PAD_SPEED * delta
		if (player_pos.y < screen_size.y-pad_size.y/2-18 and Input.is_action_pressed(get_name()+"_move_plus")):
		    player_pos.y += PAD_SPEED * delta
		
		if (player_rot > -1.3 and Input.is_action_pressed(get_name()+"_rot_clockwise")):
			player_rot += -ROT_SPEED * delta
		if (player_rot < 1.3 and Input.is_action_pressed(get_name()+"_rot_counterclockwise")):
			player_rot += ROT_SPEED * delta
	
	if (get_name() == "player2" or get_name() == "player4"):
		if (player_pos.x > 0+pad_size.y/2+26 and Input.is_action_pressed(get_name()+"_move_minus")):
			player_pos.x += -PAD_SPEED * delta
		if (player_pos.x < screen_size.x-pad_size.y/2-26 and Input.is_action_pressed(get_name()+"_move_plus")):
		    player_pos.x += PAD_SPEED * delta
		if (player_rot > 0.2 and Input.is_action_pressed(get_name()+"_rot_clockwise")):
			player_rot += -ROT_SPEED * delta
		if (player_rot < 2.8 and Input.is_action_pressed(get_name()+"_rot_counterclockwise")):
			player_rot += ROT_SPEED * delta

	set_rot(player_rot)
	set_pos(player_pos)
	
	var life = get_node("../"+get_name()+"Goal").life
	get_node("Lifes").set_frame(life)
	
	if(life <= 0):
		get_node("../"+get_name()+"Goal/Wall").set_trigger(false)
		get_node("../victory").death(get_name())
		free()
	


